FROM python:3.9.2-slim-buster

COPY ./requirements.txt .
COPY ./plugio/ /plugio
COPY ./echo_plugin /echo_plugin
COPY ./wait_plugin /wait_plugin 
COPY ./core /core
RUN pip install -r requirements.txt

CMD ["uvicorn", "core.api.main:app", "--host", "0.0.0.0", "--port", "80", "--reload"]
