""""""

# stdlib imports

# 3rd party imports

# package imports


class PlugioConfig:
    """Class for marking a Plugio plugin configuration"""

    def __init__(self, plugin_name=None, callbacks_module=None) -> None:
        self.name = plugin_name
        self.callbacks_module = callbacks_module
        self.callbacks = {}

    def __call__(self, hook, function=None):
        """Mark a function as a callback for a hook.

        :param hook: hook to register callback for
        :return: decorator_callback wrapper function
        """

        def decorator_callback(func):
            """Wrap function in decorator

            :param func: function to register as callback
            :return: func
            """

            # get all current callbacks for a hook
            callbacks_for_hook = self.callbacks.get(hook, [])

            # check that this function isn't already marked for this hook
            if func in callbacks_for_hook:
                raise TypeError(f"{func} already marked for {hook}")

            # add function to hook callbacks
            # or initialize callbacks list for this hook and add
            if callbacks_for_hook:
                self.callbacks[hook] += [func]
            else:
                self.callbacks[hook] = [func]
            return func

        return decorator_callback
