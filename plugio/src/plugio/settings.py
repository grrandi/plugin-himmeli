""""""
# stdlib imports

# 3rd party imports

# package imports


class PlugioSettings:
    def __init__(self, settings_module) -> None:
        self._SETTINGS_MODULE = settings_module
        self._settings = set()

        # module = importlib.import_module(self._SETTINGS_MODULE)
        module = self._SETTINGS_MODULE

        for setting in dir(module):
            if setting.isupper():
                setting_value = getattr(module, setting)
                setattr(self, setting, setting_value)
                self._settings.add(setting)
