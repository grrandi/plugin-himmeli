""""""

# stdlib imports
import importlib

# 3rd party imports

# package imports


class PlugioRegistry:
    """Plugin registry that collects configurations from installed plugio plugins."""

    def __init__(self, hooks=None, plugin_configs=None) -> None:
        self.hooks = hooks or []
        self._plugio_configs = plugin_configs or []
        self._configs = self._register_configs(self._plugio_configs)
        self.callbacks = {}
        self._register_callbacks()
        self._set_hooks_as_attrs()

    def register_hooks(self, hooks) -> None:
        self.hooks += hooks
        pass

    @staticmethod
    def _register_configs(configs):
        valid_configs = []
        for config in configs:
            mod_path, _, callback = config.rpartition(".")
            try:
                mod = importlib.import_module(mod_path)
                config = getattr(mod, callback)
                # get references to hook callbacks
                importlib.import_module(config.callbacks_module)
                valid_configs.append(config)
            except ImportError:
                # couldn't import config module
                continue
        return valid_configs

    def _register_callbacks(self) -> None:
        for config in self._configs:
            for hook, callbacks in config.callbacks.items():
                if hook not in self.callbacks.keys():
                    self.callbacks[hook] = []
                self.callbacks[hook] += callbacks

    def _set_hooks_as_attrs(self):
        for hook, callbacks in self.callbacks.items():
            setattr(self, hook, callbacks)
