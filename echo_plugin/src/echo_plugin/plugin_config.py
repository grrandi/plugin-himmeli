"""Echo plugin config."""

# stdlib imports

# 3rd party imports
from plugio import PlugioConfig

# package imports


echo_config = PlugioConfig("echo", "echo_plugin.main")
