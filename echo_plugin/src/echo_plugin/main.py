"""Echo plugin."""

# stdlib imports

# 3rd party imports

# package imports
from .plugin_config import echo_config


@echo_config("thing_a")
@echo_config("before_thing_a")
async def echo(*args, **kwargs):
    return args, kwargs
