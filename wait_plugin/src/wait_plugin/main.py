"""Echo plugin."""

# stdlib imports
import asyncio

# 3rd party imports

# package imports
from .plugin_config import wait_config


@wait_config("thing_a")
@wait_config("before_thing_a")
async def wait_for_2sec(*args, **kwargs):
    await asyncio.sleep(2)


@wait_config("thing_a")
@wait_config("before_thing_a")
async def wait_for_1sec(*args, **kwargs):
    await asyncio.sleep(1)


@wait_config("before_thing_a")
@wait_config("after_thing_a")
async def wait_for_3sec(*args, **kwargs):
    await asyncio.sleep(3)
