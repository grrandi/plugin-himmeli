"""Echo plugin config."""

# stdlib imports

# 3rd party imports
from plugio import PlugioConfig

# package imports


wait_config = PlugioConfig("wait", "wait_plugin.main")
