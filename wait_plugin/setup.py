from setuptools import setup
from os import path

classifiers = [
    "Development Status :: 1 - Planning" "Intended Audience :: Developers",
    "License :: OSI Approved :: MIT License",
    "Operating System :: POSIX",
    "Topic :: Software Development :: Libraries",
    "Topic :: Utilities",
    "Programming Language :: Python :: 3 :: Only",
] + [("Programming Language :: Python :: %s" % x) for x in "3 3.6 3.7 3.8 3.9".split()]

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

EXTRAS_REQUIRE = {
    "dev": ["pre-commit", "tox"],
    "testing": ["pytest"],
}


def main():
    setup(
        name="plugio-wait-plugin",
        description="Plugin that makes you wait",
        long_description=long_description,
        long_description_content_type="text/markdown",
        setup_requires=["setuptools-scm"],
        license="MIT license",
        platforms=["unix", "linux"],
        author="Matti Pohjanvirta",
        author_email="matti.pohjanvirta@iki.fi",
        url="https://gitlab.com/grrandi/wait_plugin",
        python_requires=">=3.5",
        install_requires=['importlib-metadata>=0.12;python_version<"3.8"'],
        extras_require=EXTRAS_REQUIRE,
        classifiers=classifiers,
        packages=["wait_plugin"],
        package_dir={"": "src"},
    )


if __name__ == "__main__":
    main()