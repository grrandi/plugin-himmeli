import time

from fastapi import FastAPI

from core.app import main

app = FastAPI()


@app.get("/")
async def read_root():
    start = time.time()
    ret = await main.hello_world()
    end = time.time()
    return {"start": start, "end": end, "duration": end - start, "data": ret}
