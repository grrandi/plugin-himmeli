""""""

# stdlib imports
import asyncio

# 3rd party imports
from plugio import PlugioSettings
from plugio import PlugioRegistry

# package imports
from . import settings

settings = PlugioSettings(settings)
plugio_registry = PlugioRegistry(
    hooks=[
        "before_thing_a",
        "thing_a",
        "after_thing_a",
        "before_thing_b",
        "thing_b",
        "after_thing_b",
    ],
    plugin_configs=settings.INSTALLED_PLUGINS,
)


async def hello_world():
    # before_thing_a
    args = [1, 2, 3]
    kwargs = {"hello": "world"}
    coros_before_thing_a = [
        fn(*args, **kwargs) for fn in plugio_registry.before_thing_a
    ]
    coros_before_thing_a = await asyncio.gather(
        *[fn(*args, **kwargs) for fn in plugio_registry.before_thing_a],
        return_exceptions=True,
    )

    # thing_a
    coros_thing_a = await asyncio.gather(
        *[fn(*args, **kwargs) for fn in plugio_registry.thing_a],
        return_exceptions=True,
    )

    return coros_before_thing_a, coros_thing_a
